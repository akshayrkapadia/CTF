# Investigating Windows

--------------------------------------------------------------------

## What version and year is the Windows machine

```powershell
systeminfo
```

[1](./imgs/1.png)

Windows Server 2016

## Which user last logged on

```powershell
net user
```

[2](./imgs/2.png)

Administrator

## When did John last log on

```powershell
net user john
```

[3](./imgs/3.png)

03/02/2019 5:48:32 PM

## What IP does the system connect to when it first starts

p.exe connects to 10.34.2.3 at start up

Can also check the registry key HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run

## What 2 accounts have administrative privileges

Open up properties for each user in the computer management program

Jenny, Guest

## What is the name of the malicious scheduled task and what file does it run

Go to Task Scheduler Library in the Task Scheduler program and click through all the tasks to see the actions they execute

The task is called Clean file system and it runs nc.ps1 listening on port 1348

[4](./imgs/4.png)

## 
