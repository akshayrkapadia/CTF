# Anthem

--------------------------------------------------------------------

## GIVEN INFO

**IP Address**: 10.10.174.224

--------------------------------------------------------------------

## PROCEDURE

```
nmap -sV 10.10.174.224
```

**EXPOSED PORT (SERVICE)**:<br>
    80 (Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)),<br>
    3389 (ms-wbt-server)<br>

```
gobuster dir -u http://10.10.174.224 -w /usr/share/wordlists/dirb/common.txt
```

Found /robots.txt, /Archive, /archive, /authors, /Blog, /blog, /categories, <br>

Found potential password in /robots.txt: UmbracoIsTheBest!<br>
Found other dirs: /bin, /config, /umbraco, /umbraco_client<br>

Umbraco is the CMS<br>

Website domain name is anthem.com<br>

Email scheme is: JD@anthem.com<br>

Found Flag 2 in page source: THM{G!T_G00D}<br> 

Found Flag 3 in /authors: THM{L0L_WH0_D15}<br>

Original author of poem is Solomon Grundy who is the admin<br>

Logged into umbraco with email sg@anthem.com<br>

Logged into remote desktop using Remmina on port 3389<br>
**USER**: sg<br>

Found restore file in hidden directory called backup<br>

**ROOT PASSWORD**: ChangeMeBaby1MoreTime<br>

**ROOT FLAG**: THM{Y0U_4R3_1337}