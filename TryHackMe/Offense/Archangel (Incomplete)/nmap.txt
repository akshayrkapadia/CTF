# Nmap 7.93 scan initiated Sun May  7 16:09:43 2023 as: nmap -A -p- -T4 -oN nmap.txt 10.10.173.179
Warning: 10.10.173.179 giving up on port because retransmission cap hit (6).
Nmap scan report for 10.10.173.179
Host is up (0.13s latency).
Not shown: 65478 closed tcp ports (conn-refused), 55 filtered tcp ports (no-response)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey: 
|   2048 9f1d2c9d6ca40e4640506fedcf1cf38c (RSA)
|   256 637327c76104256a08707a36b2f2840d (ECDSA)
|_  256 b64ed29c3785d67653e8c4e0481cae6c (ED25519)
80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
|_http-server-header: Apache/2.4.29 (Ubuntu)
|_http-title: Wavefire
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Sun May  7 16:22:57 2023 -- 1 IP address (1 host up) scanned in 794.39 seconds
