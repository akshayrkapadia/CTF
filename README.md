# Capture The Flag (CTF)
My notes for each CTF challenge I complete.

## Offense

**TryHackMe:**<br>
[Mr. Robot CTF](./TryHackMe/Offense/MrRobotCTF/MrRobotCTF.md)<br>
[Basic Penetration Testing](./TryHackMe/Offense/BasicPenetrationTesting/BasicPenetrationTesting.md)<br>
[Root Me](./TryHackMe/Offense/RootMe/RootMe.md)<br>
[Pickle Rick](./TryHackMe/Offense/PickleRick/PickleRick.md)<br>
[Overpass](./TryHackMe/Offense/Overpass/Overpass.md)<br>
[Agent Sudo](./TryHackMe/Offense/AgentSudo/AgentSudo.md)<br>
[Bounty Hacker](./TryHackMe/Offense/BountyHacker/BountyHacker.md)<br>
[Wonderland](./TryHackMe/Offense/Wonderland/Wonderland.md)<br>
[Simple CTF](./TryHackMe/Offense/SimpleCTF/SimpleCTF.md)<br>
[Lazy Admin](./TryHackMe/Offense/LazyAdmin/LazyAdmin.md)<br>
[Brooklyn Nine Nine](./TryHackMe/Offense/LazyAdmin/LazyAdmin.md)<br>
[Ignite](./TryHackMe/Offense/Ignite/Ignite.md)<br>
[Wgel CTF](./TryHackMe/Offense/WgelCTF/WgelCTF.md)<br>
[Startup](./TryHackMe/Offense/Startup/Startup.md)<br>
[Lian_Yu](./TryHackMe/Offense/Lian_Yu/Lian_Yu.md)<br>
[Cyborg](./TryHackMe/Offense/Cyborg/Cyborg.md)<br>
[Easy Peasy](./TryHackMe/Offense/EasyPeasy/EasyPeasy.md)<br>
[ToolsRUs](./TryHackMe/Offense/ToolsRUs/ToolsRUs.md)<br>
[Skynet](./TryHackMe/Offense/Skynet/Skynet.md)<br>
[Anonymous](./TryHackMe/Offense/Anonymous/Anonymous.md)<br>
[Boiler CTF](./TryHackMe/Offense/BoilerCTF/BoilerCTF.md)<br>
[UltraTech](./TryHackMe/Offense/UltraTech/UltraTech.md)<br>
[Chill Hack](./TryHackMe/Offense/ChillHack/ChillHack.md)<br>
[Anthem](./TryHackMe/Offense/Anthem/Anthem.md)<br>
[GamingServer](./TryHackMe/Offense/GamingServer/GamingServer.md)<br>
[h4cked](./TryHackMe/Offense/h4cked/h4cked.md)<br>
[Git Happens](./TryHackMe/Offense/GitHappens/GitHappens.md)<br>
[Break Out The Cage](./TryHackMe/Offense/BreakOutTheCage/BreakOutTheCage.md)<br>

**HackTheBox:**<br>
[Starting Point Tier 0](./HackTheBox/StartingPoint/Tier0/Tier0.md)<br>
[Starting Point Tier 1](./HackTheBox/StartingPoint/Tier1/Tier1.md)<br>
[Starting Point Tier 2](./HackTheBox/StartingPoint/Tier2/Tier2.md)<br>


## Defense

**TryHackMe:**<br>
[Carnage](./TryHackMe/Defense/Carnage/Carnage.md)<br>
[Snort](./TryHackMe/Defense/SOCLevel1/Snort/Snort.md)<br>

## Reverse Engineering

**PicoCTF:**<br>
[Bbbbloat](./PicoCTF/ReverseEngineering/Bbbbloat/Bbbbloat.md)<br>
[bloat.flag.py](./PicoCTF/ReverseEngineering/bloat-flag-py/deciphered.py)<br>
[keygenme.py](./PicoCTF/ReverseEngineering/keygenme-py/notes.txt)<br>
[crackme-py](./PicoCTF/ReverseEngineering/crackme-py/crackme_modified.py)<br>
[Picker I](./PicoCTF/ReverseEngineering/Picker-I/Picker-I.txt)<br>
[Picker II](./PicoCTF/ReverseEngineering/Picker-II/Picker-II.txt)<br>
[Picker III](./PicoCTF/ReverseEngineering/Picker-III/Picker-III.txt)<br>

